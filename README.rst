Source files for "Git for neuroscientists"

To build the documentation, you will need to install Sphinx (http://sphinx-doc.org)
and then run "make html".

Licence: CC-BY
